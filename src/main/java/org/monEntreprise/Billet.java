package org.monEntreprise;

public class Billet {
    private Integer prix;
    private String allee;
    private int rangee;

    public Billet(Integer prix) {
        this.prix = prix;
    }

    public Integer getPrix() {
        return prix;
    }


    public void setPrix(Integer prix) {
        this.prix = prix;
    }

    public void setSiegePosition(String allee, int rangee) {
        this.allee = allee;
        this.rangee = rangee;
    }

}
