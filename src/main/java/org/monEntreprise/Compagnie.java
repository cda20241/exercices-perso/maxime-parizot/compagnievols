package org.monEntreprise;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Compagnie {
    private String nom;
    private List<Vol> vols;
    private Integer numVol;

    public Compagnie(String nom) {
        this.nom = nom;
        this.numVol = 0;
        this.vols = new ArrayList<>();
    }

    public void creerVol(
            String villeDepart,
            String villeArrivee,
            Date dateDepart,
            Date dateArrivee) {
        this.numVol += 1;
        Vol vol = new Vol(numVol, dateDepart, dateArrivee, villeDepart, villeArrivee);
        this.vols.add(vol);
        vol.setCompagnie(this);
    }

    public void annulerVol(Vol vol_annule) {
        this.vols.remove(vol_annule);
        vol_annule.setCompagnie(null);
    }

    public void afficherVols() {
        for(Vol vol : this.vols) {
            System.out.println(vol);
        }
    }

    public List<Vol> getVolsByVilleDepart(String villeDepart) {
        List<Vol> listeVols = new ArrayList<>();
        for (Vol vol : this.vols) {
            if (Objects.equals(vol.getVilleDepart(), villeDepart)) {
                listeVols.add(vol);
            }
        }
        return listeVols;
    }
    public List<Vol> getVolsByNumVol(Integer numVol) {
        List<Vol> listeVols = new ArrayList<>();
        for (Vol vol : this.vols) {
            if (Objects.equals(vol.getNumVol(), numVol)) {
                listeVols.add(vol);
            }
        }
        return listeVols;
    }
}
