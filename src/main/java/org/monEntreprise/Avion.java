package org.monEntreprise;

import java.util.Scanner;

public class Avion {
    private String modele;
    private Siege[][] Sieges;
    public Avion(String modele) {
        this.modele = modele;
    }

    public String getModele() {
        return modele;
    }

    public void setQuantiteSieges(Integer nombreRangee, Integer nombreAllee) {
        /* Prend en argument le nombre de rangées (Integer) et le nombre d'allées (Integer)
         Créer une matrice composée d'objets "Siege" ayant comme valeur leurs positions relative dans la matrice,
         au format (allee = "lettre", rangee = nombre) */
        this.Sieges = new Siege[nombreRangee][nombreAllee];
        for (Integer i = 0; i < Sieges.length; i++) {
            for (Integer j = 0; j < Sieges[i].length; j++) {
                String rangee = Character.toString((char) ('A' + j));
                Siege siege = new Siege(rangee, i+1);
                Sieges[i][j] = siege;
            }
        }
    }
    public void reserveBillet(String allee, int rangee) {
        /* Prend en argument la lettre (string) de l'allée et le numéro (int) de la rangée ex: ("B",4)
        Ajoute un nouvel objet billet au siège correspondant à la position entrée par l'utilisateur */
        for (int i = 0; i < Sieges.length; i++) {
            for (int j = 0; j < Sieges[i].length; j++) {
                Siege siege = Sieges[i][j];
                if (siege.getNumRangee() == rangee && siege.getNumAllee().equals(allee)) {
                    Billet billet = new Billet(50); // Créer un billet de valeur prix:50
                    billet.setSiegePosition(siege.getNumAllee(), siege.getNumRangee());
                    //^^^ Donne la position du siège au billet
                    siege.setBillet(billet);
                    return;
                }
            }
        }
        throw new IllegalArgumentException("Aucun siege trouvé à l'emplacement " + allee + rangee);
        //^^^ Retourne une erreur si l'emplacement entré n'existe pas
    }



    public Siege[][] getSieges() {
        return Sieges;
    }
}
