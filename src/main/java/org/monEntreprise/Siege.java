package org.monEntreprise;

public class Siege {
    private String numAllee;
    private Integer numRangee;
    private Billet billet;

    public Siege(String numAllee, Integer numRangee) {
        this.numAllee = numAllee;
        this.numRangee = numRangee;
    }

    public String getNumAllee() {
        return numAllee;
    }

    public Integer getNumRangee() {
        return numRangee;
    }

    public Billet getBillet() {
        return billet;
    }

    public void setNumAllee(String numAllee) {
        this.numAllee = numAllee;
    }

    public void setNumRangee(Integer numRangee) {
        this.numRangee = numRangee;
    }

    public void setBillet(Billet billet) {
        this.billet = billet;
    }


}
