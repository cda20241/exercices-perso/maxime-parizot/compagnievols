package org.monEntreprise;

import java.util.Date;
import java.util.List;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        // Press Alt+Entrée with your caret at the highlighted text to see how
        // IntelliJ IDEA suggests fixing it.
        Compagnie compagnie1 = new Compagnie("A");
        Compagnie compagnie2 = new Compagnie("B");
        compagnie1.creerVol("Berlin", "Tokyo",
                new Date(2023, 10, 17),
                new Date(2023, 10, 18));
        compagnie1.creerVol("Paris", "Tokyo",
                new Date(2023, 10, 17),
                new Date(2023, 10, 18));
        compagnie1.afficherVols();
        List<Vol> volsParis = compagnie1.getVolsByVilleDepart("Paris");
        for (Vol vol : volsParis) {
            compagnie1.annulerVol(vol);
        }
        System.out.println("Vols pour Paris annulés");
        compagnie1.afficherVols();
        List<Vol> volsChoisis = compagnie1.getVolsByNumVol(1); // Met le vol N°1 (Berlin, Tokyo) dans une liste afin de lui envoyer des ordres
        for (Vol vol : volsChoisis) {
            vol.setAvion(new Avion("Boeing")); // Assigne un nouvel avion au vol
            vol.setQuantiteSieges(14,3); // Choisi le nombre de places présent dans l'avion
            vol.reserveBillet("C",14); // Réserve un billet dans l'avion affrété au vol
        Avion avion2 = new Avion("Boeing2");
        avion2.setQuantiteSieges(14,3);
        avion2.getSieges();
        avion2.reserveBillet("C",13);
        avion2.getSieges();
        }
    }
}